package com.i2g.callmanager;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class CallManager extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_manager);

        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        // register PhoneStateListener
        PhoneStateListener callStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int state, String incomingNumber)
            {
                //  React to incoming call.
                //number=incomingNumber;
                // If phone ringing
                if(state==TelephonyManager.CALL_STATE_RINGING)
                {
                    Toast.makeText(getApplicationContext(), "i2g Call Manager: Phone Is Riging:" + incomingNumber , Toast.LENGTH_LONG).show();

                }
                // If incoming call received
                if(state==TelephonyManager.CALL_STATE_OFFHOOK)
                {
                    Toast.makeText(getApplicationContext(),"i2g Call Manager: Phone is Currently in A call:"+ incomingNumber, Toast.LENGTH_LONG).show();
                }


                if(state==TelephonyManager.CALL_STATE_IDLE)
                {
                    Toast.makeText(getApplicationContext(),"i2g Call Manager: Phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
                }
            }
        };
       // telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_call_manager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
