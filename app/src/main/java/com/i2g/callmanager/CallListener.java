package com.i2g.callmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class CallListener extends BroadcastReceiver {
    public CallListener() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");
        try
        {


            Bundle bundle=intent.getExtras();
            if( bundle != null ){
                String state = bundle.getString(TelephonyManager.EXTRA_STATE);// =intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                String number = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                if(state.equals(TelephonyManager.EXTRA_STATE_RINGING))
                {
                    if( number != null ){
                        Toast.makeText(context,"Incoming phone number:" + number , Toast.LENGTH_LONG).show();
                    }
                }
                else if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))
                {

                    Toast.makeText(context, "Call Recieved:off hook state, number:"  + number, Toast.LENGTH_LONG).show();
                    // Your Code
                }

                else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
                {

                    Toast.makeText(context, "Phone Is Idle, number:"  + number, Toast.LENGTH_LONG).show();
                    // Your Code

                }
            }
        }
        catch(Exception e)
        {
            //your custom message
        }
    }
}
